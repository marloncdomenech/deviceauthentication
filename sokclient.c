#include <stdio.h>
#include "relic.h"
#include "relic_test.h"

#define ARQ_SKCLIENT "/home/marloncdomenech/deviceauthentication/SKCLIENT.txt"


int sokaka_clientsessionkey_gen(int8_t *key, unsigned int key_len, char *id1, int len1, char *id2, int len2) {

    sokaka_t k;
    sokaka_null(k);
    sokaka_new(k);


    FILE *fpread;
    if ((fpread = fopen(ARQ_SKCLIENT, "rt")) != NULL) {

        char stringteste1[65];
        char stringteste2[65];
        char stringteste3[65];
        char stringteste4[65];
        char stringteste5[65];
        char stringteste6[65];
        char stringteste7[65];
        char stringteste8[65];
        char stringteste9[65];
        int aux_norm, aux_norm2;

        memset(stringteste1, 0, 65);
        memset(stringteste2, 0, 65);
        memset(stringteste3, 0, 65);
        memset(stringteste4, 0, 65);
        memset(stringteste5, 0, 65);
        memset(stringteste6, 0, 65);
        memset(stringteste7, 0, 65);
        memset(stringteste8, 0, 65);
        memset(stringteste9, 0, 65);

        fscanf(fpread, "%s", &stringteste1); /* read from file */
        fscanf(fpread, "%s", &stringteste2); /* read from file */
        fscanf(fpread, "%s", &stringteste3); /* read from file */
        fscanf(fpread, "%d", &aux_norm); /* read from file */
        fscanf(fpread, "%s", &stringteste4); /* read from file */
        fscanf(fpread, "%s", &stringteste5); /* read from file */
        fscanf(fpread, "%s", &stringteste6); /* read from file */
        fscanf(fpread, "%s", &stringteste7); /* read from file */
        fscanf(fpread, "%s", &stringteste8); /* read from file */
        fscanf(fpread, "%s", &stringteste9); /* read from file */
        fscanf(fpread, "%d", &aux_norm2); /* read from file */        
        fclose(fpread);
        
        
        fp_read_str(k->s1->x, stringteste1, 64, 16);
        fp_read_str(k->s1->y, stringteste2, 64, 16);
        fp_read_str(k->s1->z, stringteste3, 64, 16);
        k->s1->norm = aux_norm;
        fp_read_str(k->s2->x[0], stringteste4, 64, 16);
        fp_read_str(k->s2->y[0], stringteste5, 64, 16);
        fp_read_str(k->s2->z[0], stringteste6, 64, 16);
        fp_read_str(k->s2->x[1], stringteste7, 64, 16);
        fp_read_str(k->s2->y[1], stringteste8, 64, 16);
        fp_read_str(k->s2->z[1], stringteste9, 64, 16);
        k->s2->norm = aux_norm2;
    }
    
    cp_sokaka_key(key, key_len, id1, len1, k, id2, len2);
    //printf("%s \n", "ok sokaka_clientsessionkey_gen");
    return 0;

}

int encrypt( uint8_t *k1, uint8_t *out_enc, int number){

	int out_len, in_num_len;
        out_len = BC_LEN + MD_LEN;
        uint8_t in_plain[BC_LEN - 1], in_number[1];

		in_number[0] = number;
        in_num_len = 1;

        //printf("%s %d\n", "in before enc= ", in_number[0]);
        /*printf("%s \n", "in before enc= ");
        for (int a = 0; a < BC_LEN - 1; a++) {
            printf("%d ", in_plain[a]);
        }*/
        //printf("\n");

        uint8_t iv[BC_LEN] = {0};

        //ENCRIPTA NO SERVIDOR

//        int i = bc_aes_cbc_enc(out_enc, &out_len, in_plain, in_len, k1, 256, iv);
        int i = bc_aes_cbc_enc(out_enc, &out_len, in_number, in_num_len, k1, 256, iv);
//        if (i == STS_OK) {
//            printf("%s \n", "encryption ok");
//        } else {
//            printf("%s \n", "encryption error");
//        }

//        printf("%s \n", "in_plain after enc= ");
        for (int a = 0; a < BC_LEN + MD_LEN; a++) {
            printf("%d ", out_enc[a]);
        }
        printf("\n");

	return 0;

}

int decrypt(uint8_t *k2, uint8_t *out_enc){

	uint8_t in_plain[BC_LEN - 1], in_number2[1];
	int out_len, in_num_len;
    out_len = BC_LEN + MD_LEN;
	in_num_len = 1;
	uint8_t iv[BC_LEN] = {0};

	int ii = bc_aes_cbc_dec(in_number2, &out_len, out_enc, out_len, k2, 256, iv);

//	if (ii == STS_OK) {
//		printf("%s \n", "decryption ok");
//	} else {
//		printf("%s \n", "decryption error");
//	}

//	printf("%s \n", "in after dec= ");
//	for (int a = 0; a < 1; a++) {
//		printf("%d ", in_number2[a]);
//		//out[a] = out[a] -1;
//	}

//      in_number2[0] = in_number2[0] -1;
	int temp  = in_number2[0];
//	printf("\ntemp novo: %d\n", temp);

//	printf("\n");

	return temp;
}

int main(int argc, char *argv[]) {

    if (core_init() != STS_OK) {
        core_clean();
        return 1;
    }

    if (pc_param_set_any() == STS_OK) {
	
		//char id_serv[5] = {'A', 'l', 'i', 'c', 'e'};
		//char id_clientnew[5] = {'B', 'a', 'r', 'i', 'a'};
	
		if(atoi(argv[1]) == 1){


			char id_client[15];

	                // TRATA DO TAMANHO DA ID DO CLIENTE
        	        int counter = 0;
                	while(argv[2][counter]!='\0')
	                {
        	                id_client[counter] = argv[2][counter];
                	        counter++;
	                }

        	        int countagain = 0;
	                char id_clientnew[counter];
        	        while(countagain < counter)
                	{
	                        id_clientnew[countagain] = id_client[countagain];
        	                countagain++;
                	}
	                // END TRATA DO TAMANHO DA ID DO CLIENTE


			char id_serv[15];
			// TRATA DO TAMANHO DA ID DO SERVIDOR
                        int counterServ = 0;
                        while(argv[3][counterServ]!='\0')
                        {
                                id_serv[counterServ] = argv[3][counterServ];
                                counterServ++;
                        }

                        int countagainServ = 0;
                        char id_servnew[counterServ];
                        while(countagainServ < counterServ)
                        {
                                id_servnew[countagainServ] = id_serv[countagainServ];
                                countagainServ++;
                        }
                        // END TRATA DO TAMANHO DA ID DO SERVIDOR


			int l = 32;
			uint8_t k2[32];

			// GERA CHAVE DE SESS�O
			sokaka_clientsessionkey_gen(k2, l, id_clientnew, sizeof(id_clientnew), id_servnew, sizeof(id_servnew));
			// END GERA CHAVE DE SESS�O

			// PEGA N�MERO CRIPTOGRAFADO E CONVERTE EM ARRAY
			uint8_t out_enc[BC_LEN + MD_LEN];
			uint8_t out_reenc[BC_LEN + MD_LEN];
			int out_enc2[BC_LEN + MD_LEN];
			for(int i = 4, j = 0; i<argc; i++, j++){
				out_enc2[j] = atoi(argv[i]);
				//printf("valor lido: %i\n", out_enc2[j]);
				out_enc[j] = out_enc2[j];
				//printf("valor lido2: %d\n", out_enc[j]);
			}

			// DECRIPTA N�MERO ALEAT�RIO COM A CHAVE DE SESS�O
			int temp = decrypt(k2, out_enc);
			//printf("%i\n", temp);

			// RESOLVE O CHALLENGE-RESPONSE
			temp = temp - 1;

			// ENCRIPTA A RESPOSTA DO CHALLENGE COM A CHAVE DE SESS�O
			encrypt(k2, out_reenc, temp);
		}
		return 0;

    }else{
        THROW(ERR_NO_CURVE);
    }

    core_clean();
    return 0;
}
