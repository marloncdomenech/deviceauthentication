#include <stdio.h>

#include "relic.h"
#include "relic_test.h"
#include <time.h>
#include <stdlib.h>

#define ARQ_SKCLIENT "/home/marloncdomenech/deviceauthentication/SKCLIENT.txt"
#define ARQ_SKSERV "/home/marloncdomenech/deviceauthentication/SKSERV.txt"

static int sokaka_masterkey_gen(bn_t s) {
    bn_null(s);
    bn_new(s);
    cp_sokaka_gen(s);

    FILE *fp;
    if (fp = fopen("/home/marloncdomenech/deviceauthentication/masterkey.txt", "w")) {
        fprintf(fp, "%d\n", s->alloc);
        fprintf(fp, "%d\n", s->used);
        fprintf(fp, "%d\n", s->sign);
        //fprintf(fp, "%i\n", BN_SIZE);
        for (int a = 0; a < BN_SIZE; a++) {
            fprintf(fp, "%llu\n", s->dp[a]);
        }
        fclose(fp);
    }

    return 0;
}

static int sokaka_serverprivatekey_gen(char *id, int id_len) {

    bn_t s;
    bn_null(s);
    bn_new(s);

    FILE *fpread;
    if ((fpread = fopen("/home/marloncdomenech/deviceauthentication/masterkey.txt", "rt")) != NULL) {
        fscanf(fpread, "%d", &s->alloc); /* read from file */
        fscanf(fpread, "%d", &s->used); /* read from file */
        fscanf(fpread, "%d", &s->sign); /* read from file */
        for (int a = 0; a < BN_SIZE; a++) {
            fscanf(fpread, "%llu", &s->dp[a]);
        }
        fclose(fpread);
    }

    sokaka_t k;
    sokaka_null(k);
    sokaka_new(k);
    cp_sokaka_gen_prv(k, id, id_len, s);

    FILE *fp;
    if (fp = fopen(ARQ_SKSERV, "w")) {

        char stringteste1[65];
        char stringteste2[65];
        char stringteste3[65];
        char stringteste4[65];
        char stringteste5[65];
        char stringteste6[65];
        char stringteste7[65];
        char stringteste8[65];
        char stringteste9[65];

        memset(stringteste1, 0, 65);
        memset(stringteste2, 0, 65);
        memset(stringteste3, 0, 65);
        memset(stringteste4, 0, 65);
        memset(stringteste5, 0, 65);
        memset(stringteste6, 0, 65);
        memset(stringteste7, 0, 65);
        memset(stringteste8, 0, 65);
        memset(stringteste9, 0, 65);

        fp_write_str(stringteste1, sizeof (stringteste1), k->s1->x, 16);
        fp_write_str(stringteste2, sizeof (stringteste2), k->s1->y, 16);
        fp_write_str(stringteste3, sizeof (stringteste3), k->s1->z, 16);
        int aux_norm = k->s1->norm;
        fp_write_str(stringteste4, sizeof (stringteste4), k->s2->x[0], 16);
        fp_write_str(stringteste5, sizeof (stringteste5), k->s2->y[0], 16);
        fp_write_str(stringteste6, sizeof (stringteste6), k->s2->z[0], 16);
        fp_write_str(stringteste7, sizeof (stringteste7), k->s2->x[1], 16);
        fp_write_str(stringteste8, sizeof (stringteste8), k->s2->y[1], 16);
        fp_write_str(stringteste9, sizeof (stringteste9), k->s2->z[1], 16);
        int aux_norm2 = k->s2->norm;

        fprintf(fp, "%s\n", stringteste1);
        fprintf(fp, "%s\n", stringteste2);
        fprintf(fp, "%s\n", stringteste3);
        fprintf(fp, "%d\n", aux_norm);
        fprintf(fp, "%s\n", stringteste4);
        fprintf(fp, "%s\n", stringteste5);
        fprintf(fp, "%s\n", stringteste6);
        fprintf(fp, "%s\n", stringteste7);
        fprintf(fp, "%s\n", stringteste8);
        fprintf(fp, "%s\n", stringteste9);
        fprintf(fp, "%d\n", aux_norm2);
        fclose(fp);
    }

    printf("%s \n", "ok sokaka_serverprivatekey_gen");
    return 0;

}

static int sokaka_privatekey_gen(char *id, int id_len) {

    bn_t s;
    bn_null(s);
    bn_new(s);

    FILE *fpread;
    if ((fpread = fopen("/home/marloncdomenech/deviceauthentication/masterkey.txt", "rt")) != NULL) {
        fscanf(fpread, "%d", &s->alloc); /* read from file */
        fscanf(fpread, "%d", &s->used); /* read from file */
        fscanf(fpread, "%d", &s->sign); /* read from file */
        for (int a = 0; a < BN_SIZE; a++) {
            fscanf(fpread, "%llu", &s->dp[a]);
        }
        fclose(fpread);
    }

    sokaka_t k;
    sokaka_null(k);
    sokaka_new(k);
    cp_sokaka_gen_prv(k, id, id_len, s);

    printf("%s \n", "CHAVE SECRETA DO CLIENTE");

    char stringteste1[65];
    char stringteste2[65];
    char stringteste3[65];
    char stringteste4[65];
    char stringteste5[65];
    char stringteste6[65];
    char stringteste7[65];
    char stringteste8[65];
    char stringteste9[65];

    memset(stringteste1, 0, 65);
    memset(stringteste2, 0, 65);
    memset(stringteste3, 0, 65);
    memset(stringteste4, 0, 65);
    memset(stringteste5, 0, 65);
    memset(stringteste6, 0, 65);
    memset(stringteste7, 0, 65);
    memset(stringteste8, 0, 65);
    memset(stringteste9, 0, 65);

    fp_write_str(stringteste1, sizeof (stringteste1), k->s1->x, 16);
    fp_write_str(stringteste2, sizeof (stringteste2), k->s1->y, 16);
    fp_write_str(stringteste3, sizeof (stringteste3), k->s1->z, 16);
    int aux_norm = k->s1->norm;
    fp_write_str(stringteste4, sizeof (stringteste4), k->s2->x[0], 16);
    fp_write_str(stringteste5, sizeof (stringteste5), k->s2->y[0], 16);
    fp_write_str(stringteste6, sizeof (stringteste6), k->s2->z[0], 16);
    fp_write_str(stringteste7, sizeof (stringteste7), k->s2->x[1], 16);
    fp_write_str(stringteste8, sizeof (stringteste8), k->s2->y[1], 16);
    fp_write_str(stringteste9, sizeof (stringteste9), k->s2->z[1], 16);
    int aux_norm2 = k->s2->norm;

    printf("%s\n", stringteste1);
    printf("%s\n", stringteste2);
    printf("%s\n", stringteste3);
    printf("%d\n", aux_norm);
    printf("%s\n", stringteste4);
    printf("%s\n", stringteste5);
    printf("%s\n", stringteste6);
    printf("%s\n", stringteste7);
    printf("%s\n", stringteste8);
    printf("%s\n", stringteste9);
    printf("%d\n", aux_norm2);
    
    
    //PARA TESTES COM ARQUIVO
    FILE *fp;
    if (fp = fopen(ARQ_SKCLIENT, "w")) {

        char stringteste11[65];
        char stringteste22[65];
        char stringteste33[65];
        char stringteste44[65];
        char stringteste55[65];
        char stringteste66[65];
        char stringteste77[65];
        char stringteste88[65];
        char stringteste99[65];

        memset(stringteste11, 0, 65);
        memset(stringteste22, 0, 65);
        memset(stringteste33, 0, 65);
        memset(stringteste44, 0, 65);
        memset(stringteste55, 0, 65);
        memset(stringteste66, 0, 65);
        memset(stringteste77, 0, 65);
        memset(stringteste88, 0, 65);
        memset(stringteste99, 0, 65);

        fp_write_str(stringteste11, sizeof (stringteste11), k->s1->x, 16);
        fp_write_str(stringteste22, sizeof (stringteste22), k->s1->y, 16);
        fp_write_str(stringteste33, sizeof (stringteste33), k->s1->z, 16);
        int aux_norm111 = k->s1->norm;
        fp_write_str(stringteste44, sizeof (stringteste44), k->s2->x[0], 16);
        fp_write_str(stringteste55, sizeof (stringteste55), k->s2->y[0], 16);
        fp_write_str(stringteste66, sizeof (stringteste66), k->s2->z[0], 16);
        fp_write_str(stringteste77, sizeof (stringteste77), k->s2->x[1], 16);
        fp_write_str(stringteste88, sizeof (stringteste88), k->s2->y[1], 16);
        fp_write_str(stringteste99, sizeof (stringteste99), k->s2->z[1], 16);
        int aux_norm2111 = k->s2->norm;

        fprintf(fp, "%s\n", stringteste11);
        fprintf(fp, "%s\n", stringteste22);
        fprintf(fp, "%s\n", stringteste33);
        fprintf(fp, "%d\n", aux_norm111);
        fprintf(fp, "%s\n", stringteste44);
        fprintf(fp, "%s\n", stringteste55);
        fprintf(fp, "%s\n", stringteste66);
        fprintf(fp, "%s\n", stringteste77);
        fprintf(fp, "%s\n", stringteste88);
        fprintf(fp, "%s\n", stringteste99);
        fprintf(fp, "%d\n", aux_norm2111);
        fclose(fp);
    }

    printf("%s \n", "ok sokaka_privatekey_gen");
    return 0;

}

static int sokaka_serversessionkey_gen(int8_t *key, unsigned int key_len, char *id1, int len1, char *id2, int len2) {

    sokaka_t k;
    sokaka_null(k);
    sokaka_new(k);


    FILE *fpread;
    if ((fpread = fopen(ARQ_SKSERV, "rt")) != NULL) {

        char stringteste1[65];
        char stringteste2[65];
        char stringteste3[65];
        char stringteste4[65];
        char stringteste5[65];
        char stringteste6[65];
        char stringteste7[65];
        char stringteste8[65];
        char stringteste9[65];
        int aux_norm = 0;
        int aux_norm2 = 0;

        memset(stringteste1, 0, 65);
        memset(stringteste2, 0, 65);
        memset(stringteste3, 0, 65);
        memset(stringteste4, 0, 65);
        memset(stringteste5, 0, 65);
        memset(stringteste6, 0, 65);
        memset(stringteste7, 0, 65);
        memset(stringteste8, 0, 65);
        memset(stringteste9, 0, 65);

        fscanf(fpread, "%s", &stringteste1); /* read from file */
        fscanf(fpread, "%s", &stringteste2); /* read from file */
        fscanf(fpread, "%s", &stringteste3); /* read from file */
        fscanf(fpread, "%d", &aux_norm); /* read from file */
        fscanf(fpread, "%s", &stringteste4); /* read from file */
        fscanf(fpread, "%s", &stringteste5); /* read from file */
        fscanf(fpread, "%s", &stringteste6); /* read from file */
        fscanf(fpread, "%s", &stringteste7); /* read from file */
        fscanf(fpread, "%s", &stringteste8); /* read from file */
        fscanf(fpread, "%s", &stringteste9); /* read from file */
        fscanf(fpread, "%d", &aux_norm2); /* read from file */        
        fclose(fpread);
        
        
        fp_read_str(k->s1->x, stringteste1, 64, 16);
        fp_read_str(k->s1->y, stringteste2, 64, 16);
        fp_read_str(k->s1->z, stringteste3, 64, 16);
        k->s1->norm = aux_norm;
        fp_read_str(k->s2->x[0], stringteste4, 64, 16);
        fp_read_str(k->s2->y[0], stringteste5, 64, 16);
        fp_read_str(k->s2->z[0], stringteste6, 64, 16);
        fp_read_str(k->s2->x[1], stringteste7, 64, 16);
        fp_read_str(k->s2->y[1], stringteste8, 64, 16);
        fp_read_str(k->s2->z[1], stringteste9, 64, 16);
        k->s2->norm = aux_norm2;
    }

    cp_sokaka_key(key, key_len, id1, len1, k, id2, len2);


    //printf("%s \n", "ok sokaka_serversessionkey_gen");
    return 0;

}

int encrypt( uint8_t *k1, uint8_t *out_enc, int number){

	int out_len, in_num_len;
        out_len = BC_LEN + MD_LEN;
        uint8_t in_plain[BC_LEN - 1], in_number[1];

		in_number[0] = number;
        in_num_len = 1;

        //printf("%s %d\n", "in before enc= ", in_number[0]);
        /*printf("%s \n", "in before enc= ");
        for (int a = 0; a < BC_LEN - 1; a++) {
            printf("%d ", in_plain[a]);
        }*/
        //printf("\n");

        uint8_t iv[BC_LEN] = {0};

        //ENCRIPTA NO SERVIDOR

//        int i = bc_aes_cbc_enc(out_enc, &out_len, in_plain, in_len, k1, 256, iv);
        int i = bc_aes_cbc_enc(out_enc, &out_len, in_number, in_num_len, k1, 256, iv);
//        if (i == STS_OK) {
//            printf("%s \n", "encryption ok");
//        } else {
//            printf("%s \n", "encryption error");
//        }

//        printf("%s \n", "in_plain after enc= ");
		//printf("encRandomNumber:");
        for (int a = 0; a < BC_LEN + MD_LEN; a++) {
            printf("%d|", out_enc[a]);
        }       

	return 0;

}

int decrypt(uint8_t *k2, uint8_t *out_enc){

	uint8_t in_plain[BC_LEN - 1], in_number2[1];
	int out_len, in_num_len;
    out_len = BC_LEN + MD_LEN;
	in_num_len = 1;
	uint8_t iv[BC_LEN] = {0};

	int ii = bc_aes_cbc_dec(in_number2, &out_len, out_enc, out_len, k2, 256, iv);

//	if (ii == STS_OK) {
//		printf("%s \n", "decryption ok");
//	} else {
//		printf("%s \n", "decryption error");
//	}

//	printf("%s \n", "in after dec= ");
//	for (int a = 0; a < 1; a++) {
//		printf("%d ", in_number2[a]);
//		//out[a] = out[a] -1;
//	}

//      in_number2[0] = in_number2[0] -1;
	int temp  = in_number2[0];
//	printf("\ntemp novo: %d\n", temp);

//	printf("\n");

	return temp;
}

int main(int argc, char *argv[]) {

    if (core_init() != STS_OK) {
        core_clean();
        return 1;
    }	
	
    if (pc_param_set_any() == STS_OK) {
	
		//char id_cc[5] = {'B', 'a', 'r', 'i', 'a'};
		//sokaka_privatekey_gen(id_cc, 5);
		
		char id_serv[5] = {'A', 'l', 'i', 'c', 'e'};
		
		//gera chave mestra
		if(atoi(argv[1]) == 3){
		  bn_t s;
		  sokaka_masterkey_gen(s);
		  
		  return 0;
		}
		
		//gera chave privada do IdP
		if(atoi(argv[1]) == 4){
		  sokaka_serverprivatekey_gen(id_serv, sizeof(id_serv));
		  return 0;
		}
		
		//gera chave privada para fase de registro
		if(atoi(argv[1]) == 5){		  
		  
		  char id_client[15];
		  			
		  // TRATA DO TAMANHO DA ID DO CLIENTE
		  int counter = 0;
		  while(argv[2][counter]!='\0')
		  {
			  id_client[counter] = argv[2][counter];	
			  counter++;
		  }
			
		  int countagain = 0;
		  char id_clientnew[counter];
		  while(countagain < counter)
		  {
			  id_clientnew[countagain] = id_client[countagain];
			  countagain++;
		  }			
		  // END TRATA DO TAMANHO DA ID DO CLIENTE
		  
		  sokaka_privatekey_gen(id_clientnew, sizeof(id_clientnew));
		  		  
		  return 0;
		}
		
	
		if(atoi(argv[1]) == 1){			
			
			printf("%i|", 1);
			char id_client[15];
			int l = 32;
			uint8_t k1[32], k2[32];
			
			
			// TRATA DO TAMANHO DA ID DO CLIENTE
			int counter = 0;
			while(argv[2][counter]!='\0')
			{
				id_client[counter] = argv[2][counter];	
				counter++;
			}
			
			int countagain = 0;
			char id_clientnew[counter];
			while(countagain < counter)
			{
				id_clientnew[countagain] = id_client[countagain];
				countagain++;
			}			
			// END TRATA DO TAMANHO DA ID DO CLIENTE
			
			// GERA CHAVE DE SESS�O
			sokaka_serversessionkey_gen(k1, l, id_serv, sizeof(id_serv), id_clientnew, sizeof(id_clientnew));
			// END GERA CHAVE DE SESS�O
			
			
			/* TESTA GERA��O DE CHAVE DE SESS�O
			
			sokaka_clientsessionkey_gen(k2, l, id_clientnew, sizeof(id_clientnew), id_serv, sizeof(id_serv));

			if (memcmp(k1, k2, l) == 0) {
				printf("%s \n", "Chaves de sessao iguais");
			printf("k1 = \n");
			for(int count = 0; count < 32; count++){
				printf("%i  ", k1[count]);
			}
			printf("k2 = \n");
					for(int count = 0; count < 32; count++){
							printf("%i  ", k2[count]);
					}

			} else {
				printf("%s \n", "Chaves de sessao diferentes");
			}
			*/
			
			
			// GERA E ENCRIPTA N�MERO ALEAT�RIO COM A CHAVE DE SESS�O DO SERVIDOR
			srand(time(NULL));
			int r = rand() % 256;
			printf("%i|", r);
			
			uint8_t out_enc[BC_LEN + MD_LEN];
			encrypt(k1, out_enc, r);
			printf("#\n");
			// END GERA E ENCRIPTA N�MERO ALEAT�RIO COM A CHAVE DE SESS�O DO SERVIDOR				
		}else{
						
			char id_client[15];
			int l = 32;
			uint8_t k1[32], k2[32];
						
			// TRATA DO TAMANHO DA ID DO CLIENTE
			int counter = 0;
			while(argv[2][counter]!='\0')
			{
				id_client[counter] = argv[2][counter];	
				counter++;
			}
			
			int countagain = 0;
			char id_clientnew[counter];
			while(countagain < counter)
			{
				id_clientnew[countagain] = id_client[countagain];
				countagain++;
			}			
			// END TRATA DO TAMANHO DA ID DO CLIENTE
			
			// GERA CHAVE DE SESS�O
			sokaka_serversessionkey_gen(k1, l, id_serv, sizeof(id_serv), id_clientnew, sizeof(id_clientnew));
			// END GERA CHAVE DE SESS�O
			
			// PEGA N�MERO CRIPTOGRAFADO E CONVERTE EM ARRAY
			uint8_t out_enc[BC_LEN + MD_LEN];
			int out_enc2[BC_LEN + MD_LEN];
			for(int i = 3, j = 0; i<argc; i++, j++){
				out_enc2[j] = atoi(argv[i]);
				//printf("valor lido: %i\n", out_enc2[j]);
				out_enc[j] = out_enc2[j];
				//printf("valor lido2: %d\n", out_enc[j]);				
			}
			
			// DECRIPTA N�MERO ALEAT�RIO COM A CHAVE DE SESS�O DO SERVIDOR		
			int temp = decrypt(k1, out_enc);			
			printf("%i\n", temp);
			
		}
		
		return 0;
	
	
    }else{
        THROW(ERR_NO_CURVE);        
    }
    
    core_clean();
    return 0;
}
